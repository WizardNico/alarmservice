﻿using AlarmService.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace AlarmService.Data
{
   public class DataContext : DbContext
   {
      public DbSet<Alarm> Alarms { get; set; }

      protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
      {
         optionsBuilder.UseSqlServer(@"Data Source=localhost;Initial Catalog=AlarmService;Persist Security Info=True;User ID=sa;Password=nico93;Pooling=False;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True");
      }

      protected override void OnModelCreating(ModelBuilder modelBuilder)
      {
      }
   }
}
