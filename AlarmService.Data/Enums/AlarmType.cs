﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlarmService.Data.Enums
{
   public enum AlarmType
   {
      Fire = 10,
      Intrusion = 20,
   }
}
