﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlarmService.Data.Enums
{
   public enum AlarmPriority
   {
      High = 100,
      Medium = 50,
      Low = 10,
   }
}
