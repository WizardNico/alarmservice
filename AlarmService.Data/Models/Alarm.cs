﻿using AlarmService.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AlarmService.Data.Models
{
   public class Alarm
   {
      [Required]
      public long Id { get; set; }



      [Required]
      public AlarmPriority Priority { get; set; }

      [Required]
      public AlarmType Type { get; set; }

      [Required]
      public DateTime Date { get; set; }



      [Required]
      public long RoomId { get; set; }
   }
}
