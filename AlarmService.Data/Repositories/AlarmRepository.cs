﻿using AlarmService.Data.Enums;
using AlarmService.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlarmService.Data.Repositories
{
   public static class AlarmRepository
   {
      public static async Task<Alarm> FindByIdAsync(this DbSet<Alarm> repository, long id)
      {
         Alarm alarm = await repository
           .Where(alrm => alrm.Id == id)
           .SingleOrDefaultAsync();

         return (alarm);
      }

      public static async Task<List<Alarm>> GetListAsync(this DbSet<Alarm> repository, AlarmType? type, long? roomId)
      {
         IQueryable<Alarm> query = repository;

         if (type != null)
         {
            query = query.Where(alrm => alrm.Type == type);
         }

         if (roomId != null)
         {
            query = query.Where(alrm => alrm.RoomId == roomId);
         }

         List<Alarm> result = await query
            .ToListAsync();

         return (result);
      }
   }
}
