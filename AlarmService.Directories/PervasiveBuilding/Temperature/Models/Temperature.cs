﻿using AgileMQ.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AlarmService.Directories.PervasiveBuilding.Temperature.Models
{
   [QueuesConfig(Directory = "PervasiveBuilding", Subdirectory = "Temperature", ResponseEnabled = true)]
   public class Temperature
   {
      [Required]
      public long? Id { get; set; }



      public DateTime? InsertionDate { get; set; }

      [Required]
      public double? TemperatureValue { get; set; }

      [Required]
      public double? HumidityValue { get; set; }



      [Required]
      public long? RoomId { get; set; }
   }
}
