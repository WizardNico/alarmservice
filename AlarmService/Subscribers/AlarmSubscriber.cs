﻿using AgileMQ.Interfaces;
using DataModels = AlarmService.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using AgileMQ.Containers;
using AgileMQ.DTO;
using System.Threading.Tasks;
using AlarmService.Data;
using AlarmService.Directories.PervasiveBuilding.Alarm.Models;
using AlarmService.Directories.PervasiveBuilding.Room.Models;
using AlarmService.Data.Enums;
using AlarmService.Utilities;
using AlarmService.Directories.PervasiveBuilding.Employee.Models;
using AlarmService.Directories.PervasiveBuilding.Employee.Enum;
using AlarmService.Data.Exceptions;
using AlarmService.Data.Repositories;
using AgileMQ.Extensions;

namespace AlarmService.Subscribers
{
   public class AlarmSubscriber : IAgileSubscriber<Alarm>
   {
      public IAgileBus Bus { get; set; }

      public async Task<Alarm> GetAsync(Alarm model, MessageContainer container)
      {
         DataContext ctx = container.Resolve<DataContext>();

         DataModels.Alarm alarm = await ctx.Alarms.FindByIdAsync(model.Id.Value);

         if (alarm == null)
            throw new ObjectNotFoundException("Alarm not found, a wrong ID was used.");

         model = Mapper.Map(alarm);

         return (model);
      }

      public async Task<List<Alarm>> GetListAsync(Dictionary<string, object> filter, MessageContainer container)
      {
         DataContext ctx = container.Resolve<DataContext>();

         AlarmType? type = filter.Get<AlarmType?>("alarmType");
         long? roomId = filter.Get<long?>("roomId");

         List<Alarm> list = new List<Alarm>();

         List<DataModels.Alarm> alarmList = await ctx.Alarms.GetListAsync(type, roomId);

         foreach (DataModels.Alarm alarm in alarmList)
         {
            list.Add(Mapper.Map(alarm));
         }

         return (list);
      }

      private bool IsWeekend()
      {
         if ((DateTime.Today.DayOfWeek == DayOfWeek.Saturday) || (DateTime.Today.DayOfWeek == DayOfWeek.Sunday))
            return true;
         else
            return false;
      }

      private bool IsWorkingHour()
      {
         if (DateTime.Now.Hour >= 7 && DateTime.Now.Hour < 18)
            return true;
         else
            return false;
      }

      public async Task FireDetectedAsync(Alarm model, MessageContainer container)
      {
         DataContext ctx = container.Resolve<DataContext>();

         //Get the indicated room
         Room room = await Bus.GetAsync(new Room() { Id = model.RoomId });

         //Notify security staff building's
         List<Employee> securityStaffList = await Bus.GetListAsync<Employee>(new Dictionary<string, object> { { "roomId", null }, { "preference", null }, { "role", EmployeeRole.Security } });
         Parallel.ForEach(securityStaffList, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, security =>
         {
            Mail.smtp(Project.EmailSenderHost, Project.EmailSenderAddress, Project.EmailPassword, Project.EmailSenderName, security.Email, Project.FireSubject, Project.FireMessageSecurity(security.Name, room.RoomNumber.Value, room.FloorNumber.Value));
         });

         //If it happens during a weekday, in a working hour, notify also the employees for the evacuation
         if (!IsWeekend() && IsWorkingHour())
         {
            //Get all the rooms on the indicated floor to evacuate 'em all
            List<Room> roomList = await Bus.GetListAsync<Room>(new Dictionary<string, object> { { "floorNumber", room.FloorNumber }, { "roomNumber", null } });
            Parallel.ForEach(roomList, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount / 2 }, async r =>
            {
               //Get list of employee of this room
               List<Employee> employeeList = await Bus.GetListAsync<Employee>(new Dictionary<string, object> { { "roomId", r.Id }, { "preference", null }, { "role", null } });
               Parallel.ForEach(employeeList, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount / 2 }, employee =>
               {
                  if (employee.Role != EmployeeRole.Security)
                  {
                     if (employee.RoomId == model.RoomId)
                        Mail.smtp(Project.EmailSenderHost, Project.EmailSenderAddress, Project.EmailPassword, Project.EmailSenderName, employee.Email, Project.FireSubject, Project.RoomFireMessage(employee.Name, r.FireEscape.Code, r.FireEscape.Location));
                     else
                        Mail.smtp(Project.EmailSenderHost, Project.EmailSenderAddress, Project.EmailPassword, Project.EmailSenderName, employee.Email, Project.FireSubject, Project.FloorFireMessage(employee.Name, r.FireEscape.Code, r.FireEscape.Location));
                  }
               });
            });
         }

         //Cretion of new temperature record
         DataModels.Alarm alarm = new DataModels.Alarm()
         {
            Date = DateTime.Now,
            RoomId = room.Id.Value,
            Priority = (AlarmPriority)model.Priority,
            Type = AlarmType.Fire,
         };

         //Add the new record to DB adn save changes
         await ctx.Alarms.AddAsync(alarm);
         await ctx.SaveChangesAsync();
      }

      public async Task IntrusionDetectedAsync(Alarm model, MessageContainer container)
      {
         DataContext ctx = container.Resolve<DataContext>();

         //Get the indicated room
         Room room = await Bus.GetAsync(new Room() { Id = model.RoomId });

         List<Employee> securityStaffList = await Bus.GetListAsync<Employee>(new Dictionary<string, object> { { "roomId", null }, { "preference", null }, { "role", EmployeeRole.Security } });
         Parallel.ForEach(securityStaffList, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, security =>
          {
             Mail.smtp(Project.EmailSenderHost, Project.EmailSenderAddress, Project.EmailPassword, Project.EmailSenderName, security.Email, Project.IntrusionSubject, Project.IntrusionMessage(room.RoomNumber.Value, room.FloorNumber.Value));
          });

         //Cretion of new temperature record
         DataModels.Alarm alarm = new DataModels.Alarm()
         {
            Date = DateTime.Now,
            RoomId = room.Id.Value,
            Priority = (AlarmPriority)model.Priority,
            Type = AlarmType.Intrusion,
         };

         //Add the new record to DB adn save changes
         await ctx.Alarms.AddAsync(alarm);
         await ctx.SaveChangesAsync();
      }


      public Task DeleteAsync(Alarm model, MessageContainer container)
      {
         throw new NotImplementedException();
      }

      public Task<Page<Alarm>> GetPagedListAsync(Dictionary<string, object> filter, long pageIndex, int pageSize, MessageContainer container)
      {
         throw new NotImplementedException();
      }

      public Task<Alarm> PostAsync(Alarm model, MessageContainer container)
      {
         throw new NotImplementedException();
      }

      public Task PutAsync(Alarm model, MessageContainer container)
      {
         throw new NotImplementedException();
      }
   }
}
