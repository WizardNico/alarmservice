﻿using AgileMQ.Bus;
using AgileMQ.Enums;
using AgileMQ.Interfaces;
using AlarmService.Data;
using AlarmService.Logger;
using AlarmService.Subscribers;
using System;

namespace AlarmService
{
   public class Program
   {
      static void Main(string[] args)
      {
         using (IAgileBus bus = new RabbitMQBus("HostName=localhost;Port=5672;UserName=guest;Password=guest;Timeout=3;RetryLimit=3;PrefetchCount=50;AppId=AlarmService"))
         {
            bus.Logger = new ConsoleLogger();

            bus.Container.Register<DataContext, DataContext>(InjectionScope.Message);

            bus.Suscribe(new AlarmSubscriber());

            Console.WriteLine("AlarmService Ready!");
            Console.WriteLine();
            Console.ReadLine();
         }
      }
   }
}
