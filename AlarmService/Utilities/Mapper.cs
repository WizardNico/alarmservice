﻿using PervasiveAlarmModels = AlarmService.Directories.PervasiveBuilding.Alarm.Models;
using DataModels = AlarmService.Data.Models;
using AlarmService.Directories.PervasiveBuilding.Alarm.Enum;

namespace AlarmService.Utilities
{
   public class Mapper
   {
      public static PervasiveAlarmModels.Alarm Map(DataModels.Alarm alarm)
      {
         return new PervasiveAlarmModels.Alarm()
         {
            Id = alarm.Id,
            Date = alarm.Date,
            Priority = (AlarmPriority)alarm.Priority,
            Type = (AlarmType)alarm.Type,
            RoomId = alarm.RoomId,
         };
      }
   }
}
