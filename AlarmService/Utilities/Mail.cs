﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace AlarmService.Utilities
{
   public class Mail
   {
      public static void smtp(string senderHost, string senderAddress, string password, string senderName, string receiverAddress, string subject, string message)
      {
         //creo l'e-mail
         SmtpClient client = new SmtpClient();
         client.Port = 587;
         client.Host = "smtp.gmail.com";
         client.EnableSsl = true;
         client.Timeout = 10000;
         client.DeliveryMethod = SmtpDeliveryMethod.Network;
         client.UseDefaultCredentials = false;

         MailAddress from = new MailAddress(senderAddress, senderName, System.Text.Encoding.UTF8);
         MailAddress to = new MailAddress(receiverAddress);
         MailMessage email = new MailMessage(from, to);
         email.Subject = subject;
         email.Body = message;
         email.BodyEncoding = System.Text.Encoding.UTF8;
         email.SubjectEncoding = System.Text.Encoding.UTF8;

         //credenziali
         if (password != null)
            client.Credentials = new System.Net.NetworkCredential(senderAddress, password);

         //spedisco l'e-mail
         client.Send(email);
      }
   }
}
