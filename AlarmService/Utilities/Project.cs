﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlarmService.Utilities
{
   public class Project
   {
      public static string EmailSenderHost { get { return ("smtp.gmail.com"); } }
      public static string EmailSenderAddress { get { return ("pervasivebuilding@gmail.com"); } }
      public static string EmailPassword { get { return ("ubipervasive"); } }
      public static string EmailSenderName { get { return ("Pervasive-Building"); } }
      public static string EmailAdministrationAddress { get { return ("andrea.nicolini93@gmail.com"); } }
      private static string FireEmoji { get { return ("\uDBB9\uDCF6"); } }
      private static string DetectiveEmoji { get { return ("\uD83D\uDD75\uD83C\uDFFB\u200D\u2642\uFE0F"); } }
      public static string FireSubject { get { return ("[" + FireEmoji + "FIRE ALARM" + FireEmoji + "]"); } }
      public static string IntrusionSubject { get { return ("[" + DetectiveEmoji + "INTRUSION ALARM" + DetectiveEmoji + "]"); } }
      public static string IntrusionMessage(int RoomNumber, int FloorNumber) => "A suspected intrusion is happening in room " + RoomNumber + " on floor " + FloorNumber;
      public static string FloorFireMessage(string Name, string FireEscapeCode, string FireEscapeLocation) => "Hi " + Name + ", it seems like there is a fire in your floor, please exit the building as soon as possible via the fire door " + FireEscapeCode + ", located at/in: " + FireEscapeLocation;
      public static string FireMessageSecurity(string Name, int RoomNumber, int FloorNumber) => "Hi " + Name + ", it seems like there is a fire in room " + RoomNumber + " on floor " + FloorNumber + ", your intervention is required!";
      public static string RoomFireMessage(string Name, string FireEscapeCode, string FireEscapeLocation) => "Hi " + Name + ", it seems like there is a fire in your room, please exit the building as soon as possible via the fire door " + FireEscapeCode + ", located at/in: " + FireEscapeLocation;
   }
}
